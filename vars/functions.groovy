void awsLogin(String loginType,String registry){
    sh " aws ecr get-login-password --region us-east-1 | ${loginType} login --username AWS --password-stdin $registry"
}

void getVersion(){
    def version=env.BRANCH_NAME.replaceAll("/","-")
    return "${version}-${env.BUILD_NUMBER}"
}

void getTag(String version, String registry){
    def tag
    def repo = msName()
    return tag="${registry}/${repo}:${version}"
}

void msName(){
    return env.JOB_NAME.replace("CI/","").replace("/feature%2Freconfigure","")
}

void chechoutGlobalChart(){
    def remoteBranchExist=true
    sshagent(credentials: ['gitlabssh']) {
        sh 'mkdir -p ~/.ssh'
        sh 'echo "Host *" > ~/.ssh/config' 
        sh 'echo "  StrictHostKeyChecking no" > ~/.ssh/config'
        sh "git config --global --add safe.directory ${WORKSPACE}/tempCheckout"
    try{
        remoteBranchExist = sh(returnStdout: true, script: "git ls-remote --heads git@gitlab.com:googlems/webstore.git | grep -w ${env.BRANCH_NAME}")
    }catch(err){
        remoteBranchExist=false
    }
    }
    sh  "git clone -b main git@gitlab.com:googlems/webstore.git ."
    if (remoteBranchExist){
        sh  "git checkout ${env.BRANCH_NAME}"
        sh  "git config pull.rebase false"
        sh  "git pull"
    }else{
        sh  "git checkout -b ${env.BRANCH_NAME}"
    }
}

void updateMsVersionInGlobalChart(String version, String msName){
    def chartYaml = readYaml file: "Chart.yaml"
    echo "values before:"
    sh "cat Chart.yaml"
    def tmp = chartYaml.version
    chartYaml['dependencies'].each{ chart -> 
        if (chart.name == msName ){
            chart.version = version            
        }  
    }
        chartYaml.version = tmp
        echo "The chart after: $chartYaml"
        sh 'mv Chart.yaml Chart.yaml.org'
        writeYaml file: 'Chart.yaml', data: chartYaml

}

void pushGlobalChart(){
    sh "rm -rf *tgz"
    sh " helm package ."
    sh"helm push *.tgz oci://${registry}"

}

void commitChanges(){
    sshagent(credentials: ['gitlabssh']) {
        sh "git config --global user.email 'aziztrainingacc@gmail.com'"
        sh "git config --global user.name 'azizaf'"
        sh "git add Chart.yaml values.yaml"
        sh "git commit -m 'jenkins update version'"
        sh "git push -f origin '${env.BRANCH_NAME}'"
    }
}